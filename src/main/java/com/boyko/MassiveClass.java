package com.boyko;

/**
 * <h1>This is main class </h1>
 *
 */

public class MassiveClass {
    public static void main(String[] args) {

        /**
         * We create an array of eleven numbers
         *  and create cycle for in order for write odd number
         */

        int[] number = new int[11];

        for (int a = 0; a <= number.length; a++) {
            if (a % 2 != 0) {
                int c;
                c = +a++;
                System.out.println(c);

            }
        }

        /**
         * This method we count even and odd number
         * and print their
         */
        int sumOdd =0;
        int sumEven = 0;
        for(int b =0 ; b<=number.length; b++){
            if(b%2!=0){
                sumOdd+= b;
            }
            else{
                sumEven+=b;
            }
        }

        System.out.println("Odd sum "+ sumOdd);
        System.out.println("Even sum"+ sumEven);

        /**
         * Now start second part where we count Fibonacci number
         */
        System.out.println(" ");
        System.out.println("Fibonacci number");
        System.out.println(" ");
    /**
     * Add variables and find fibonacci number from eleven numbers
     */
        int number1 =1;
        int number2 =1;
        int number3;

        System.out.println(number1+" "+ number2+" ");

        for (int i =3; i<=11 ;i++){
            number3=number1+number2;
            System.out.println(number3+" ");
            number1=number2;
            number2=number3;

        }
        System.out.println();


    }

}
